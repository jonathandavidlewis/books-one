from django.contrib import admin
from django.urls import path
from booksapp.views import show_books, create_book, show_book_details, update_book

urlpatterns = [
    path("create/", create_book, name="create_book"),
    path("<int:pk>/", show_book_details, name="show_book_details"),
    # path("<int:pk>/delete/", delete_book, name="delete_book"),
    path("<int:pk>/edit/", update_book, name="update_book_view"),
    path("", show_books,  name="show_books"),
]
