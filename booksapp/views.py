from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from booksapp.forms import BookForm
from booksapp.models import Book


def show_book_details(request, pk):
    book = Book.objects.get(pk=pk)
    context = {
        "book": book
    }
    return render(request, "books/detail.html", context)


def show_books(request):
    books = Book.objects.all()
    context = {
        "books": books
    }
    return render(request, "books/list.html", context)

def create_book(request):

    form = BookForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("show_books")

    context ={}
    context['form'] = form
    return render(request, "books/create.html", context)

def update_book(request, pk):
    book = get_object_or_404(Book, pk=pk)
    
    form = BookForm(request.POST or None, instance=book)
    if form.is_valid():
        form.save()
        return redirect("show_book_details", pk=pk)

    context ={}
    context['form'] = form
    return render(request, "books/update.html", context)