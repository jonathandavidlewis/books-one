from django.contrib import admin
from booksapp.models import Author, Book, BookReview
# Register your models here.

admin.site.register(Book)
admin.site.register(BookReview)
admin.site.register(Author)


