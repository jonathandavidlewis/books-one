from django.db import models





class  Book(models.Model):
    title = models.CharField(max_length=200, unique=True)
    authors = models.ManyToManyField("Author", related_name="books", blank=True)
    pages = models.SmallIntegerField(null=True)
    isbn = models.BigIntegerField(null=True)
    year_published = models.SmallIntegerField(null=True)
    description = models.TextField(null=True)
    image = models.URLField(null=True, blank=True)
    in_print = models.BooleanField(null=True)

    def __str__(self):
        return self.title + " by " + str(self.authors.first())

class  Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name



class  BookReview(models.Model):
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()